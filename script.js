//3
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'})
	.then((response) => response.json())
	.then((json) => console.log(json));

//4

fetch('https://jsonplaceholder.typicode.com/todos')
	.then ((response) => response.json())
	.then(json => {
        json.map((item) => {
        console.log('title: ' + item.title) 
  })
})

// 5

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'})
	.then((response) => response.json())
	.then((json) => console.log(json));

// 6
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'})
	.then((response) => response.json())
	.then((json) => console.log("Item " + json.title + " has a status of " + json.completed));

//7 FETCH / POST

fetch ('https://jsonplaceholder.typicode.com/todos/',{
	method: 'POST',
	header:{
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: 'New list',
		body: 'To do list',
		completed: false	
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

//8 PUT
fetch ('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	header:{
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: 'New post',
		desciption: 'Update list',
		body: 'To do list',
		status: 'Pending',
		completed: false,
		date_completed: 'Pending'	
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// Patch
fetch ('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	header:{
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: 'New post',
		desciption: 'Update list',
		body: 'To do list',
		status: 'Complete',
		completed: true,
		date_completed: '05/31/2022'	
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE

fetch ('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE' })